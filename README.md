# WeatherAppVueJS

Aplicacion web para obtener datos del clima de cualquier ciudad del mundo.

## Demo

Si quieres ver una demo de este proyecto, puede visitar el siguiente link: [Demo](https://weatherapp14.netlify.com/)

## Construido con

- VueJS.
- API de OpenWeather. [Link al sitio](https://openweathermap.org/api)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Vista previa

![](demo.gif)
