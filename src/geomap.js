<script
  src="https://cdn.jsdelivr.net/gh/bigdatacloudapi/js-reverse-geocode-client@latest/bigdatacloud_reverse_geocode.min.js"
  type="text/javascript"
></script>;

let reverseGeocoder = new BDCReverseGeocode();
reverseGeocoder.localityLanguage = "en";
reverseGeocoder.getClientLocation(function(result) {
  return result.principalSubdivision + ", " + result.countryCode;
});
