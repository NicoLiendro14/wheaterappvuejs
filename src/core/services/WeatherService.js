const API_KEY = "b160990c7c1197807f7d4e75966f64d4";
const URL_BASE = "https://api.openweathermap.org/data/2.5/";

export default class WeatherService {
    async getWeatherOn(location) {
        let response = await fetch(`${URL_BASE}weather?q=${location}&units=metric&APPID=${API_KEY}`);

        return await response.json();
    }
}