const URL_BASE_MYLOCATION = "https://api.bigdatacloud.net/data/reverse-geocode-client?";

export default class LocationService {
    /** 
     * Obtiene el nombre de la localidad en base a las geoposición.
     * 
     * @param lat La latitud
     * @param lng La longitud
     * 
     * @returns String
     */
    async getLocation(lat, lng) {
        const query = `${URL_BASE_MYLOCATION}latitude=${lat}&longitude=${lng}&localityLanguage=es`;

        let response = await fetch(query);
        let location = await response.json();

        return location.locality + ", " + location.countryCode;
    }
}